<?php

namespace Hadrien\Utils\Converters;

interface Converter
{
    public function getFormattedUnite($unite);

    public static function from(float $value, $unite): self;

    public function to($unite): float;

    public function convertInDefaultUnite(float $value, $unite): float;
}
