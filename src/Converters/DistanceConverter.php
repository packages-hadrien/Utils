<?php

namespace Hadrien\Utils\Converters;

use Hadrien\Utils\Converters\Units\DistanceUnits;

class DistanceConverter extends ConverterAbstract
{

    public function getFormattedUnite($unite): DistanceUnits
    {
        $unite = $unite instanceof DistanceUnits ? $unite : DistanceUnits::tryFrom($unite);

        if(!$unite) {
            throw new \Exception('The unit is incorrect');
        }

        return $unite;
    }

    public static function from(float $value, $unite): DistanceConverter
    {
        return new self($value, $unite);
    }

    public function to($unite): float
    {
        return $this->value / $this->getFormattedUnite($unite)->meterCoefficient();
    }

    public function convertInDefaultUnite(float $value, $unite): float
    {
        return $value * $this->getFormattedUnite($unite)->meterCoefficient();
    }
}
