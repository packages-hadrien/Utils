<?php

namespace Hadrien\Utils\Http;

use Exception;

class Curl
{
    private string $url;
    private Request $request;
    private array $errors = [];
    private string|bool $response;

    public function __construct(string $url, $request = Request::GET)
    {
        $this->url = $url;
        $this->request = $this->setRequest($request);
    }

    public static function get(string $url): Curl
    {
        return new self($url);
    }

    public static function post(string $url): Curl
    {
        return new self($url, Request::POST);
    }

    public static function put(string $url): Curl
    {
        return new self($url, Request::PUT);
    }

    public static function delete(string $url): Curl
    {
        return new self($url, Request::DELETE);
    }

    public function setRequest(string|Request $request): Request
    {
        if($request instanceof Request) {
            return $request;
        }

        $request = Request::tryFrom($request);

        if(!$request) {
            throw new Exception("Unknown request");
        }

        return $request;
    }

    public function execute(): self
    {
        $curl = curl_init ($this->url);

        curl_setopt_array($curl, [
            CURLOPT_CAINFO => __DIR__ . '/../public/cacert.pem',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => $this->request->value,
        ]);

        $this->response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $this->errors[] = 'CURL Error ' . $err;
        }

        return $this;
    }

    public function errors(): array
    {
        return $this->errors;
    }

    public function response(): string
    {
        if($this->response) {
            return $this->response;
        }

        return $this->execute()->response;
    }


    public function jsonResponse()
    {
        return json_decode($this->response());
    }
}
