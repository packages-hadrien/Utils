<?php

namespace Hadrien\Utils\Helpers;

class ArrayHelper
{
    public array $values;

    public function __construct(array $values = [])
    {
        $this->values = $values;
    }

    public static function make(array $values = []): ArrayHelper
    {
        return new self($values);
    }

    public function first()
    {
        return reset($this->values);
    }

    public function last()
    {
        return end($this->values);
    }
}