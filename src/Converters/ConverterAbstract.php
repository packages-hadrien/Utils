<?php

namespace Hadrien\Utils\Converters;

abstract class ConverterAbstract implements Converter
{
    protected float $value;
    protected $unite;

    public function __construct(float $value, $unite)
    {
        $this->value = $this->convertInDefaultUnite($value, $unite);
        $this->unite = $this->getFormattedUnite($unite);
    }
}
