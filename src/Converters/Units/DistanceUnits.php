<?php

namespace Hadrien\Utils\Converters\Units;

enum DistanceUnits: string
{
    case GIGAMETERS = 'Gm';
    case MEGAMETERS = 'Mm';
    case KILOMETERS = 'km';
    case HECTOMETERS = 'hm';
    case DECAMETERS = 'dam';
    case METERS = 'm';
    case DECIMETERS = 'dm';
    case CENTIMETERS = 'cm';
    case MILLIMETERS = 'mm';
    case MICROMETERS = 'µm';
    case NANOMETERS = 'nm';
    case DEGREE = '°';
    case INCH = 'in';
    case FOOT = 'ft';
    case YARD = 'yd';
    case MILES = 'mi';

    public function meterCoefficient()
    {
        return match ($this) {
            self::GIGAMETERS => 1000000000,
            self::MEGAMETERS => 1000000,
            self::KILOMETERS => 1000,
            self::HECTOMETERS => 100,
            self::DECAMETERS => 10,
            self::METERS => 1,
            self::DECIMETERS => .1,
            self::CENTIMETERS => .01,
            self::MILLIMETERS => .001,
            self::MICROMETERS => .000001,
            self::NANOMETERS => .000000001,
            self::INCH => .0254,
            self::FOOT => 0.3048,
            self::YARD => 0.91,
            self::MILES => 1609.344,
            self::DEGREE => 111000,
        };
    }
}
