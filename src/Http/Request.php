<?php

namespace Hadrien\Utils\Http;

enum Request: string
{
    case GET = 'GET';
    case POST = 'POST';
    case PUT = 'PUT';
    case DELETE = 'DELETE';
}